from django.shortcuts import render
from django.shortcuts import redirect
from django.http.response import HttpResponse
# 現在日時を取得するためにインポート
from datetime import datetime
# from django.utils import timezone
# forms.pyからフォーム情報取得
from .forms import MemberForm
# models.pyからDB情報取得
from .models import Member

# ここの変数はurls.pyの設定で利用される
def index_template(request):
    # CSSの即時反映用に現在日時を格納
    now = datetime.now().strftime("%Y%m%d%H%M%S")

    # テンプレートに渡す変数を格納（辞書）
    myapp_data = {
        'lang': 'Python',
        'flame': 'Django',
        'now': now,
    }
    # 第二引数にはテンプレートのファイル名、第三引数には変数名を格納した配列？を指定
    return render(request, 'index.html', myapp_data)

def test(request):
    # CSSの即時反映用に現在日時を格納
    now = datetime.now().strftime("%Y%m%d%H%M%S")
    # DB情報取得
    data = Member.objects.all()

    # テンプレートに渡す変数を格納（辞書）
    context = {
        'title': 'member情報一覧画面',
        'now': now,
        'data': data,
    }
    return render(request, 'test.html', context)
    # return render(request, 'test.html')

def create(request):
    if request.method == 'POST':
        # なぜ一番上に記載してあるimportだけでなく、ここに記載しなければ動作しないのか分からない
        # models.pyからDB情報取得
        from .models import Member

        obj = Member()
        Member = MemberForm(request.POST, instance=obj)
        Member.save()
        # リダイレクト
        return redirect(to='./test')

    params = {
        'title': 'member情報登録画面',
        'form': MemberForm(),
    }
    return render(request, 'create.html', params)