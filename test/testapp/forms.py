from django import forms
from .models import Member
 
 
class MemberForm(forms.ModelForm):
    class Meta:
        model = Member
        fields = ['name', 'gender', 'age']
        # fields = ['name', 'gender', 'age', 'updated_at', 'created_at', 'deleted_at']