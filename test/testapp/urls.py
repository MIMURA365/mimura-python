from django.urls import path
from . import views

# ここでルーティングの設定を行う
urlpatterns = [
    path('', views.index_template, name='index'),
    path('test', views.test, name='test'),
    path('create', views.create, name='create'),
]