#!/home/mimura36/.local/share/virtualenvs/test-Ij0LhYoQ/bin/python3.7
import sys, os
 
sys.path.insert(0, "/home/mimura36/.local/share/virtualenvs/test-Ij0LhYoQ/bin")
 
os.environ['DJANGO_SETTINGS_MODULE'] = "xserver_test.settings"
 
from wsgiref.handlers import CGIHandler
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
CGIHandler().run(application)